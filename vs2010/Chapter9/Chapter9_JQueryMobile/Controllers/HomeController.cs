﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Chapter9_jQueryMobile.Models;

namespace Chapter9_JQueryMobile.Controllers
{
  public class HomeController : Controller
  {
    public ActionResult Index()
    {
      ViewBag.Message = "Modify this template to kick-start your ASP.NET MVC application.";

      return View();
    }

    public ActionResult Lists()
    {
      List<Movie> MovieList = new List<Movie>();
      MovieList.Add(new Movie(1, "Alien vs. Terminator", "Who would win this battle?  Bet on Arnold.", "Action", 2004, "Director.png"));
      MovieList.Add(new Movie(2, "The Hunger Games of Kataan", "Fad vs. Fad", "Action", 2012, "Director.png"));
      MovieList.Add(new Movie(3, "Raiders of the Lost Phone", "Couches will be destroyed", "Adventure", 2007, "Director.png"));
      
      return View(MovieList);
    }

    public ActionResult About()
    {
      ViewBag.Message = "Your app description page.";

      return View();
    }
    public ActionResult Details()
    {
      return View();
    }
    public ActionResult Purchase()
    {
      return View();
    }
  }
}
