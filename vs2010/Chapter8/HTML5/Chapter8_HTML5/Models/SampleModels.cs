﻿using System.ComponentModel.DataAnnotations;
using System;

namespace Chapter8_HTML5.Models
{
    public class SampleModel
    {
        [DataType(DataType.PhoneNumber)]
        [Display(Name = "Phone", Prompt = "Phone:")]
        public string Phone { get; set; }

        [DataType(DataType.Date)]
        public DateTime ContactDate { get; set; }

        [DataType(DataType.DateTime)]
        public DateTime ContactDateTime { get; set; }

        [DataType(DataType.Url)]
        public string WebSite { get; set; }

        [DataType(DataType.EmailAddress)]
        public string EmailAddr { get; set; }


        public string SearchTxt { get; set; }

    }
}